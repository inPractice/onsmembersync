﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using OnsMemberSync.DataModels;
using OnsMemberSync.Exceptions;
using OnsMemberSync.OnsApi;
using OnsMemberSync.Utilities;

using Common.Logging;
using Kendo.Mvc.Extensions;
using log4net.Config;

namespace OnsMemberSync
{
    internal class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger("RollingFileLogger");

        private static readonly ILog EmailLogger = LogManager.GetLogger("EmailLogger");

        private static List<GlobalAlternateId> OnsMemberList
        {
            get;
            set;
        }

        private static List<OnsMemberMetadata> OnsMemberMetadataList
        {
            get;
            set;
        }

        private static DateTime LastUpdateDate
        {
            get;
            set;
        }

        private static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            ConfigurationManager.RefreshSection("appSettings");

            Logger.Info(Constants.INFO_ONS_SYNC_BEGIN);
            Console.WriteLine(Constants.INFO_ONS_SYNC_BEGIN);

            using (var mcpEntities = new MemberCentralProdEntities())
            {
                OnsMemberList = (from a in mcpEntities.GlobalAlternateIds
                                 where a.Source == Constants.OnsGlobalAlternateIdSource
                                 select a).ToList<GlobalAlternateId>();
                OnsMemberMetadataList = mcpEntities.OnsMemberMetadatas.ToList<OnsMemberMetadata>();
            }

            if (OnsMemberList == null) { return; }

            LastUpdateDate = (from x in OnsMemberMetadataList
                              select x.Created).DefaultIfEmpty(DateTime.MinValue).Max().GetValueOrDefault();
            var onsClientsMetadataList = new List<OnsSyncClient>();
            var failedRetrievalMemberList = new List<Guid>();
            int failedRemoteIdCount = 0;
            try
            {
                SyncOnsMembers(onsClientsMetadataList, failedRetrievalMemberList, ref failedRemoteIdCount);
                Logger.Info(Constants.Formatted.INFO_ONS_SYNC_SUMMARY.FormatWith(OnsMemberList.Count,OnsMemberList.Count - failedRemoteIdCount,failedRemoteIdCount));
                var insertedRowCount = InsertOnsMemberMetadata(onsClientsMetadataList, failedRetrievalMemberList);
                Logger.Info(Constants.Formatted.INFO_ONS_DB_INSERT_COUNT.FormatWith(insertedRowCount));
                var updatedRowCount = UpdateSiteAccessMemberSubscription(onsClientsMetadataList, failedRetrievalMemberList);
                Logger.Info(Constants.Formatted.INFO_ONS_DB_UPDATE_COUNT.FormatWith(updatedRowCount));
            }
            catch (Exception e)
            {
                Logger.Error(Constants.Formatted.ERR_ONS_EXCEPTION.FormatWith(e));
            }
            Logger.Info(Constants.INFO_ONS_SYNC_END);
            string logsummary = Constants.Formatted.INFO_ONS_SYNC_SUMMARY.FormatWith(OnsMemberList.Count,OnsMemberList.Count - failedRemoteIdCount,failedRemoteIdCount);
            EmailLogger.Info(logsummary);
        }

        private static int SyncOnsMembers(ICollection<OnsSyncClient> onsClientsMetadataList, ICollection<Guid> failedRetrievalMemberList, ref int failedRemoteIdCount)
        {
            int processedMemberCount = 0;
            foreach (var onsMember in OnsMemberList)
            {
                try
                {
                    OnsSyncClient onsClientMetadata = OnsApiInfo.GetOnsClientInfo(onsMember.RemoteId);
                    if (onsClientMetadata == null)
                    {
                        throw new Exception();
                    }
                    onsClientMetadata.MemberId = onsMember.LocalId;
                    onsClientsMetadataList.Add(onsClientMetadata);
                }
                catch (ClientNotFoundException e)
                {
                    failedRetrievalMemberList.Add(onsMember.LocalId);
                    Logger.Error(Constants.Formatted.ERR_ONS_EXCEPTION.FormatWith(e));
                    failedRemoteIdCount++;
                }
                processedMemberCount++;
                if (processedMemberCount % 50 == 0)
                {
                    Console.WriteLine("{0} members processed..", processedMemberCount);
                }
            }
            return processedMemberCount;
        }

        private static int UpdateSiteAccessMemberSubscription(IEnumerable<OnsSyncClient> onsClientsMetadataList, IEnumerable<Guid> failedRetrievalMemberList)
        {
            List<Guid> inactiveMemberList = (from member in onsClientsMetadataList
                                             where !member.Active
                                             select member into m
                                             select m.MemberId).ToList<Guid>();
            inactiveMemberList.AddRange(failedRetrievalMemberList);
            int groupId;
            if (!int.TryParse(Constants.SiteAccessMemberSubscriptionGroupAccessId, out groupId))
            {
                throw new Exception(Constants.ERR_ONS_INVALID_GAID);
            }
            using (SiteAccessEntities saEntities = new SiteAccessEntities())
            {
                foreach (Guid memberId in inactiveMemberList)
                {
                    saEntities.usp_ONS_UpdateMemberSubscription(memberId, Constants.SiteAccessMemberSubscriptionTransactionId, groupId);
                }
            }
            return inactiveMemberList.Count;
        }

        private static int InsertOnsMemberMetadata(IEnumerable<OnsSyncClient> onsClientsMetadataList, IEnumerable<Guid> failedRetrievalMemberList)
        {
            int rowCount = 0;
            IEnumerable<OnsSyncClient> newAndUpdatedClientList = GetNewAndUpdatedClients(onsClientsMetadataList);
            List<OnsMemberMetadata> onsMemberMetadataList = new List<OnsMemberMetadata>();
            onsMemberMetadataList.AddRange(from onsClient in newAndUpdatedClientList
                                           select new OnsMemberMetadata
                                           {
                                               MemberId = onsClient.MemberId,
                                               IsPayingMember = new bool?(onsClient.Active)
                                           });
            using (IEnumerator<Guid> enumerator = failedRetrievalMemberList.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    Guid memberGuid = enumerator.Current;
                    if (!onsMemberMetadataList.Any((OnsMemberMetadata member) => member.MemberId == memberGuid && member.FailedRetrievalFromRemoteServer))
                    {
                        onsMemberMetadataList.Add(new OnsMemberMetadata
                        {
                            MemberId = memberGuid,
                            IsPayingMember = new bool?(false),
                            FailedRetrievalFromRemoteServer = true
                        });
                    }
                }
            }
            using (MemberCentralProdEntities mcpEntities = new MemberCentralProdEntities())
            {
                mcpEntities.OnsMemberMetadatas.AddRange(onsMemberMetadataList);
                mcpEntities.SaveChanges();
                rowCount = onsMemberMetadataList.Count;
            }
            return rowCount;
        }

        private static IEnumerable<OnsSyncClient> GetNewAndUpdatedClients(IEnumerable<OnsSyncClient> onsClientsMetadataList)
        {
            return (from onsClient in onsClientsMetadataList
                    where IsNewClient(onsClient) || HasActiveStatusChanged(onsClient)
                    select onsClient).ToList<OnsSyncClient>();
        }

        private static bool HasActiveStatusChanged(OnsSyncClient onsClient)
        {
            bool? isPayingMember = (from x in OnsMemberMetadataList
                                    where x.MemberId == onsClient.MemberId
                                    select x into y
                                    orderby y.MemberId
                                    select y into z
                                    select z.IsPayingMember).FirstOrDefault<bool?>();
            return !onsClient.Active.Equals(isPayingMember);
        }

        private static bool IsNewClient(OnsSyncClient onsClient)
        {
            return OnsMemberList.Any((GlobalAlternateId x) => x.Created > LastUpdateDate && x.LocalId == onsClient.MemberId);
        }
    }

}
