﻿using System.Configuration;

namespace OnsMemberSync.Utilities
{
    public static class Constants
    {
        public static class Formatted
        {
            public static readonly string ERR_ONS_API_CLIENT = ConfigurationManager.AppSettings["ERR_ONS_API_CLIENT"];

            public static readonly string ERR_ONS_API_CLIENT_ID = ConfigurationManager.AppSettings["ERR_ONS_API_CLIENT_ID"];

            public static readonly string ERR_ONS_EXCEPTION = ConfigurationManager.AppSettings["ERR_ONS_EXCEPTION"];

            public static readonly string INFO_ONS_DB_INSERT_COUNT = ConfigurationManager.AppSettings["INFO_ONS_DB_INSERT_COUNT"];

            public static readonly string INFO_ONS_DB_UPDATE_COUNT = ConfigurationManager.AppSettings["INFO_ONS_DB_UPDATE_COUNT"];

            public static readonly string INFO_ONS_SYNC_SUMMARY = ConfigurationManager.AppSettings["INFO_ONS_SYNC_SUMMARY"];
        }

        public static readonly string OnsApiBaseUrl = ConfigurationManager.AppSettings["OnsApiBaseUrl"];

        public static readonly string OnsApiClientIdKey = ConfigurationManager.AppSettings["OnsApiClientIdKey"];

        public static readonly string OnsApiClientIdVal = ConfigurationManager.AppSettings["OnsApiClientIdVal"];

        public static readonly string OnsApiClientSecretKey = ConfigurationManager.AppSettings["OnsApiClientSecretKey"];

        public static readonly string OnsApiClientSecretVal = ConfigurationManager.AppSettings["OnsApiClientSecretVal"];

        public static readonly string OnsApiMediaType = ConfigurationManager.AppSettings["OnsApiMediaType"];

        public static readonly string OnsGlobalAlternateIdSource = ConfigurationManager.AppSettings["OnsGlobalAlternateIdSource"];

        public static readonly string INFO_ONS_SYNC_BEGIN = ConfigurationManager.AppSettings["INFO_ONS_SYNC_BEGIN"];

        public static readonly string INFO_ONS_SYNC_END = ConfigurationManager.AppSettings["INFO_ONS_SYNC_END"];

        public static readonly string ERR_ONS_INVALID_GAID = ConfigurationManager.AppSettings["ERR_ONS_INVALID_GAID"];

        public static readonly string SiteAccessMemberSubscriptionGroupAccessId = ConfigurationManager.AppSettings["SiteAccessMemberSubscriptionGroupAccessId"];

        public static readonly string SiteAccessMemberSubscriptionTransactionId = ConfigurationManager.AppSettings["SiteAccessMemberSubscriptionTransactionId"];
    }

}
