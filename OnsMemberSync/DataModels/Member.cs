//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnsMemberSync.DataModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Member
    {
        public Member()
        {
            this.Member1 = new HashSet<Member>();
            this.OnsMemberMetadatas = new HashSet<OnsMemberMetadata>();
        }
    
        public System.Guid MemberID { get; set; }
        public Nullable<int> SalutationID { get; set; }
        public string FirstName { get; set; }
        public string Middle { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Affiliation { get; set; }
        public Nullable<int> DegreeID { get; set; }
        public Nullable<int> SpecialtyID { get; set; }
        public Nullable<int> ProfessionID { get; set; }
        public Nullable<int> StateLicensedID { get; set; }
        public string NursingIDNumber { get; set; }
        public int SourceID { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime Created { get; set; }
        public Nullable<System.DateTime> Updated { get; set; }
        public Nullable<int> LegacyMemberID { get; set; }
        public bool IsDeceased { get; set; }
        public bool IsPhoneActive { get; set; }
        public bool IsTextActive { get; set; }
        public bool IsFaxActive { get; set; }
        public bool IsIndustry { get; set; }
        public Nullable<System.Guid> PreferredMemberID { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string NABPePIDNumber { get; set; }
        public string NPIRegistryNumber { get; set; }
        public string FPDMemberNumber { get; set; }
        public Nullable<int> MedicalCertificateId { get; set; }
        public Nullable<int> MedicalCertificateSubspecialtyId { get; set; }
        public Nullable<int> GraduationYear { get; set; }
        public long MemberInt { get; set; }
        public bool HasFellowshipID { get; set; }
        public string HPCSANumber { get; set; }
    
        public virtual ICollection<Member> Member1 { get; set; }
        public virtual Member Member2 { get; set; }
        public virtual ICollection<OnsMemberMetadata> OnsMemberMetadatas { get; set; }
    }
}
