﻿using System;
using System.Text;

using Newtonsoft.Json;

namespace OnsMemberSync.OnsApi
{
    public class OnsSyncClient
    {
        public string ClientId
        {
            get;
            set;
        }

        public Guid MemberId
        {
            get;
            set;
        }

        [JsonProperty("active")]
        public bool Active
        {
            get;
            set;
        }

        public override string ToString()
        {
            var onsClientStringBuilder = new StringBuilder();
            onsClientStringBuilder.AppendFormat("[Client: [Id:{0}; Member Id:{1}; Active: {2}]]", this.ClientId, this.MemberId, this.Active);
            return onsClientStringBuilder.ToString();
        }
    }
}
