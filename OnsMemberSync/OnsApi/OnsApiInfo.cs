﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

using OnsMemberSync.Exceptions;
using OnsMemberSync.Utilities;

using Kendo.Mvc.Extensions;

namespace OnsMemberSync.OnsApi
{
    public class OnsApiInfo
    {
        private static string BaseUrl
        {
            get
            {
                return Constants.OnsApiBaseUrl;
            }
        }

        private static string ClientIdKey
        {
            get
            {
                return Constants.OnsApiClientIdKey;
            }
        }

        private static string ClientIdValue
        {
            get
            {
                return Constants.OnsApiClientIdVal;
            }
        }

        private static string ClientSecretKey
        {
            get
            {
                return Constants.OnsApiClientSecretKey;
            }
        }

        private static string ClientSecretValue
        {
            get
            {
                return Constants.OnsApiClientSecretVal;
            }
        }

        private static string MediaType
        {
            get
            {
                return Constants.OnsApiMediaType;
            }
        }

        public static OnsSyncClient GetOnsClientInfo(string clientId)
        {
            OnsSyncClient onsClient = null;
            var client = new HttpClient { BaseAddress = new Uri(BaseUrl) };
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaType));
            client.DefaultRequestHeaders.Add(ClientIdKey, ClientIdValue);
            client.DefaultRequestHeaders.Add(ClientSecretKey, ClientSecretValue);
            // List data response.
            var response = client.GetAsync(clientId).Result;// Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                using (var content = response.Content)
                {
                    // Note: Requires Microsoft.AspNet.WebApi.Client
                    onsClient = content.ReadAsAsync<OnsSyncClient>().Result;
                }
            }
            else
            {
                var innerException = new Exception(response.ToString());

                if (response.StatusCode == HttpStatusCode.NotFound && string.Equals(response.ReasonPhrase, "Not Found", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw ClientNotFoundException(Constants.Formatted.ERR_ONS_API_CLIENT.FormatWith(clientId), innerException);
                }
                throw LoggedException(Constants.Formatted.ERR_ONS_API_CLIENT.FormatWith(clientId), null);
            }

            if (onsClient == null)
            {
                throw LoggedException(Constants.Formatted.ERR_ONS_API_CLIENT_ID.FormatWith(clientId), null);
            }

            onsClient.ClientId = clientId;
            return onsClient;
        }

        private static Exception LoggedException(string message, Exception innerException)
        {
            return innerException != null ? new Exception(message, innerException) : new Exception(message);
        }

        private static Exception ClientNotFoundException(string message, Exception innerException)
        {
            return innerException != null ? new ClientNotFoundException(message, innerException) : new ClientNotFoundException(message);
        }
    }
}
